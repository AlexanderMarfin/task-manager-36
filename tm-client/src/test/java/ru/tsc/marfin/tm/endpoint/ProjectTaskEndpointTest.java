package ru.tsc.marfin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.marfin.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.marfin.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.marfin.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.marfin.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.marfin.tm.dto.response.TaskUnbindFromProjectResponse;
import ru.tsc.marfin.tm.marker.SoapCategory;
import ru.tsc.marfin.tm.model.Project;
import ru.tsc.marfin.tm.model.Task;

@Category(SoapCategory.class)
public class ProjectTaskEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private String token;

    @Nullable
    private Project initProject;

    @Nullable
    private Task initTask;

    @Before
    public void init() {
        token = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        initProject =
                projectEndpoint.createProject(new ProjectCreateRequest(token, "init project", "init")).getProject();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        initTask =
                taskEndpoint.createTask(new TaskCreateRequest(token, "init task", "init")).getTask();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest("", initProject.getId(), initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, "", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, initProject.getId(), "")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, "123", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, initProject.getId(), "123")
                )
        );
        @NotNull final TaskBindToProjectResponse response =
                projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, initProject.getId(), initTask.getId()));
        @NotNull final Task task = taskEndpoint.showTaskById(new TaskShowByIdRequest(token, initTask.getId())).getTask();
        Assert.assertEquals(initProject.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest("", initProject.getId(), initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, "", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, initProject.getId(), "")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, "123", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, initProject.getId(), "123")
                )
        );
        @NotNull final TaskUnbindFromProjectResponse response =
                projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(token, initProject.getId(), initTask.getId()));
        @NotNull final Task task = taskEndpoint.showTaskById(new TaskShowByIdRequest(token, initTask.getId())).getTask();
        Assert.assertNull(task.getProjectId());
    }

}
