package ru.tsc.marfin.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable Task task;

}
