package ru.tsc.marfin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlFasterXmlLoadRequest extends AbstractUserRequest {

    public DataXmlFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
